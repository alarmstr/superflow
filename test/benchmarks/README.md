The following log information is stored for the samples listed below:
1) stdout and stderr from SuperflowAna (`*FlatNt.log`)
2) only warnings and erros from SuperflowAna (`*FlatNt_warnings.log`)
3) cutflow results from SuperflowAna (`*FlatNt_cutflow.log`)

# Samples
- DATA15 : `user.alarmstr.data15_13TeV.00280950.physics_Main.SusyNt.p3704_n0308_nt`
- DATA16 : `user.alarmstr.data16_13TeV.00302872.physics_Main.SusyNt.p3704_n0308_nt`
- DATA17 : `user.alarmstr.data17_13TeV.00337705.physics_Main.SusyNt.p3704_n0308_nt`
- DATA18 : `user.alarmstr.data18_13TeV.00350880.physics_Main.SusyNt.p3704_n0308_nt`
- MC16A  : `user.alarmstr.mc16_13TeV.364161.Sherpa221_Wmunu_70_140_BFilt.SusyNt.mc16a.p3703_n0308_nt`
- MC16D  : `user.alarmstr.mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.SusyNt.mc16d.p3703_n0308_nt`
- MC16E  : `user.alarmstr.mc16_13TeV.364253.Sherpa_222_lllv.SusyNt.mc16e.p3703_n0308_nt`

# Replicating tests
The tests require setting up a test directory with links to the needed files and executables.
```bash
cd run
mkdir -p SuperflowBenchmarks/prev_results
cd SuperflowBenchmarks
ln -s ../../source/superflow/test/run_flatnt_tests.sh
ln -s ${path_to_test_samples} test_susyNt_samples
./run_flatnt_tests.sh -s data18 -o prev_results
./run_flatnt_tests.sh -s mc16e -o prev_results
```
If these quick tests run without crashing, then one can run the full test
```bash
./run_flatnt_tests.sh -s all -o prev_results
```
The symlink `test_susyNt_samples` is hard coded into the `run_flatnt_tests.sh` script.
Make sure to use the same name or to change the hard coded value.

Diff the files in `prev_results/` with those found in in [Superflow/test/benchmarks](Superflow/test/benchmarks).
Once all differences are understood or fixed, overwrite all the old log files
```bash
cp run/SuperflowBenchmarks/prev_results/*log source/superflow/test/benchmarks/
```

