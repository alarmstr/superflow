////////////////////////////////////////////////////////////////////////////////
/// Copyright (c) <2018> by Taffard UC Irvine group
///
/// @file SuperflowAna.cxx
/// @author UC Irvine ATLAS Group
/// @date <December 2018>
/// @brief Example superflow looper
///
///////////////////////////////////////////////////////////////////////////////

// std
#include <cstdlib>
#include <cmath>
#include <fstream>
#include <iostream>
#include <string>
#include <getopt.h>

// ROOT
#include "TChain.h"
#include "TVectorD.h"

// SusyNtuple
#include "SusyNtuple/ChainHelper.h"
#include "SusyNtuple/string_utils.h"
#include "SusyNtuple/SusyNtSys.h"
#include "SusyNtuple/KinematicTools.h"
#include "SusyNtuple/AnalysisType.h"

// Superflow
#include "Superflow/Superflow.h"
#include "Superflow/Superlink.h"
#include "Superflow/Cut.h"
#include "Superflow/StringTools.h"
#include "Superflow/input_options.h"


using namespace std;
using namespace sflow;

////////////////////////////////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////////////////////////////////
string m_ana_name = "SuperflowAna";
string m_input_ttree_name = "susyNt";
bool m_verbose = true;
bool m_print_weighted_cutflow = true;
int m_lumi = 1000; // inverse picobarns (ipb)
Susy::AnalysisType m_ana_type = Susy::AnalysisType::Ana_2Lep;

////////////////////////////////////////////////////////////////////////////////
// Declarations
////////////////////////////////////////////////////////////////////////////////
TChain* create_new_chain(string input, string ttree_name, bool verbose);
Superflow* create_new_superflow(SFOptions sf_options, TChain* chain);
void set_global_variables(Superflow* sf);
void add_cleaning_cuts(Superflow* sf);
void add_analysis_cuts(Superflow* sf);
void add_event_variables(Superflow* sf);
void add_trigger_variables(Superflow* sf);
void add_lepton_variables(Superflow* sf);
void add_jet_variables(Superflow* sf);
void add_met_variables(Superflow* sf);
void add_dilepton_variables(Superflow* sf);
void add_super_razor_variables(Superflow* sf);
void add_miscellaneous_variables(Superflow* sf);

void add_weight_systematics(Superflow* sf);
void add_shape_systematics(Superflow* sf);

// globals for use in superflow cuts and variables
static int m_cutflags = 0;
static JetVector m_light_jets;
static TLorentzVector m_dileptonP4;
static TLorentzVector m_MET;
static Susy::Lepton* m_lept1;
static Susy::Lepton* m_lept2;
static Susy::Electron *m_el0, *m_el1;
static Susy::Muon *m_mu0, *m_mu1;
static LeptonVector m_triggerLeptons;
static map<string, bool> m_triggerPass;

// helpful functions
bool is_1lep_trig_matched(Superlink* sl, string trig_name, LeptonVector leptons, float pt_min = 0);
bool is_2lep_trig_matched(Superlink* sl, string trig_name, Susy::Lepton* lep1, Susy::Lepton* lep2, float pt_min1 = 0, float pt_min2 = 0);
#define ADD_LEP_TRIGGER_VAR(trig_name) { \
    *sf << NewVar(#trig_name" trigger bit"); { \
        *sf << HFTname(#trig_name); \
        *sf << [=](Superlink* /*sl*/, var_bool*) -> bool { \
            return m_triggerPass.at(#trig_name); \
        }; \
        *sf << SaveVar(); \
    } \
}

////////////////////////////////////////////////////////////////////////////////
// Main function
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[])
{
    /////////////////////////////////////////////////////////////////////
    // Read in the command-line options (input file, num events, etc...)
    ////////////////////////////////////////////////////////////////////
    SFOptions options(argc, argv);
    options.ana_name = m_ana_name;
    if(!read_options(options)) {
        exit(1);
    }

    // New TChain* added to heap, remember to delete later
    TChain* chain = create_new_chain(options.input, m_input_ttree_name, m_verbose);

    Long64_t tot_num_events = chain->GetEntries();
    options.n_events_to_process = (options.n_events_to_process < 0 ? tot_num_events : options.n_events_to_process);

    ////////////////////////////////////////////////////////////
    // Initialize & configure the analysis
    //  > Superflow inherits from SusyNtAna : TSelector
    ////////////////////////////////////////////////////////////
    Superflow* superflow = create_new_superflow(options, chain);

    cout << options.ana_name << "    Total Entries: " << chain->GetEntries() << endl;
    //if (options.run_mode == SuperflowRunMode::single_event_syst) superflow->setSingleEventSyst(nt_sys_);

    // Set variables for use in other cuts/vars. MUST ADD FIRST!
    set_global_variables(superflow);

    // Event selections
    add_cleaning_cuts(superflow);
    add_analysis_cuts(superflow);

    // Output variables
    add_event_variables(superflow);
    add_trigger_variables(superflow);
    add_lepton_variables(superflow);
    add_jet_variables(superflow);
    add_met_variables(superflow);
    add_dilepton_variables(superflow);
    add_super_razor_variables(superflow);
    add_miscellaneous_variables(superflow);

    // Systematics
    add_weight_systematics(superflow);
    add_shape_systematics(superflow);

    // Run Superflow
    chain->Process(superflow, options.input.c_str(), options.n_events_to_process);

    // Clean up
    delete superflow;
    delete chain;

    cout << m_ana_name << "    Done." << endl;
    exit(0);
}

////////////////////////////////////////////////////////////////////////////////
// Function definitions
////////////////////////////////////////////////////////////////////////////////
TChain* create_new_chain(string input, string input_ttree_name, bool verbose) {
    TChain* chain = new TChain(input_ttree_name.c_str());
    chain->SetDirectory(0);
    ChainHelper::addInput(chain, input, verbose);
    return chain;
}
Superflow* create_new_superflow(SFOptions sf_options, TChain* chain) {
    Superflow* sf = new Superflow(); // initialize the superflow
    sf->setAnaName(sf_options.ana_name);
    sf->setAnaType(m_ana_type);
    sf->setLumi(m_lumi);
    sf->setSampleName(sf_options.input);
    sf->setRunMode(sf_options.run_mode);
    sf->setCountWeights(m_print_weighted_cutflow);
    sf->setChain(chain);
    sf->setDebug(sf_options.dbg);
    sf->nttools().initTriggerTool(ChainHelper::firstFile(sf_options.input, 0.0));
    if (sf_options.output_name!="") sf->setOutput(sf_options.output_name);
    if (sf_options.suffix_name != "") { sf->setFileSuffix(sf_options.suffix_name); }
    if (sf_options.sumw_file_name != "") {
        cout << sf_options.ana_name
             << "    Reading sumw for sample from file: "
             << sf_options.sumw_file_name << endl;
        sf->setUseSumwFile(sf_options.sumw_file_name);
    }
    return sf;
}
void set_global_variables(Superflow* sf) {
    *sf << CutName("read in") << [](Superlink* sl) -> bool {
        ////////////////////////////////////////////////////////////////////////
        // Reset all globals used in cuts/variables
        m_cutflags = 0;
        m_light_jets.clear();
        m_dileptonP4 = {};
        m_el0 = m_el1 = 0;
        m_mu0 = m_mu1 = 0;
        m_triggerLeptons.clear();
        m_triggerPass.clear();

        ////////////////////////////////////////////////////////////////////////
        // Set globals
        // Note: No cuts have been applied so add appropriate checks before
        //       dereferencing pointers or accessing vector indices
        m_cutflags = sl->nt->evt()->cutFlags[NtSys::NOM];

        // Light jets: jets that are neither forward nor b-tagged
        for (int i = 0; i < (int)sl->jets->size(); i++) {
            if ( !sl->tools->jetSelector().isBJet(sl->jets->at(i))
              && !sl->tools->jetSelector().isForward(sl->jets->at(i))) {
                m_light_jets.push_back(sl->jets->at(i));
            }
        }

        // Missing transverse momentum
        m_MET.SetPxPyPzE(sl->met->Et * cos(sl->met->phi),
                         sl->met->Et * sin(sl->met->phi),
                         0.,
                         sl->met->Et);

        // Commonly used leptons
        if (sl->leptons->size() >= 1) {
            m_lept1 = sl->leptons->at(0);
            m_triggerLeptons.push_back(m_lept1);
            if (sl->leptons->size() >= 2) {
                m_lept2 = sl->leptons->at(1);
                m_dileptonP4 = *m_lept1 + *m_lept2;
                m_triggerLeptons.push_back(m_lept2);
            }
        }

        // Set globals for dilepton trigger matching
        for (Susy::Lepton* lep : m_triggerLeptons) {
            if (lep->isEle()) {
                if (!m_el0) m_el0 = static_cast<Susy::Electron*>(lep);
                else if (!m_el1) m_el1 = static_cast<Susy::Electron*>(lep);
            }
            else if (lep->isMu()) {
                if (!m_mu0) m_mu0 = static_cast<Susy::Muon*>(lep);
                else if (!m_mu1) m_mu1 = static_cast<Susy::Muon*>(lep);
            }
        }
        // Triggers
        // For DF dilepton trig matching, the electron should always be the first
        // lepton parameter provided
        ////////////////////////////////////////////////////////////////////////////
        // 2015
        m_triggerPass.emplace("HLT_e24_lhmedium_L1EM20VH", is_1lep_trig_matched(sl, "HLT_e24_lhmedium_L1EM20VH", m_triggerLeptons, 25));
        m_triggerPass.emplace("HLT_e60_lhmedium", is_1lep_trig_matched(sl, "HLT_e60_lhmedium", m_triggerLeptons, 61));
        m_triggerPass.emplace("HLT_e120_lhloose", is_1lep_trig_matched(sl, "HLT_e120_lhloose", m_triggerLeptons, 121));

        m_triggerPass.emplace("HLT_2e12_lhloose_L12EM10VH", is_2lep_trig_matched(sl, "HLT_2e12_lhloose_L12EM10VH", m_el0, m_el1, 13, 13));

        m_triggerPass.emplace("HLT_mu20_iloose_L1MU15", is_1lep_trig_matched(sl, "HLT_mu20_iloose_L1MU15", m_triggerLeptons, 21));
        m_triggerPass.emplace("HLT_mu40", is_1lep_trig_matched(sl, "HLT_mu40", m_triggerLeptons, 41));

        m_triggerPass.emplace("HLT_2mu10", is_2lep_trig_matched(sl, "HLT_2mu10", m_mu0, m_mu1, 11, 11));
        m_triggerPass.emplace("HLT_mu18_mu8noL1", is_2lep_trig_matched(sl, "HLT_mu18_mu8noL1", m_mu0, m_mu1, 19, 9));

        m_triggerPass.emplace("HLT_e17_lhloose_mu14", is_2lep_trig_matched(sl, "HLT_e17_lhloose_mu14", m_el0, m_mu0, 18, 15));
        m_triggerPass.emplace("HLT_e7_lhmedium_mu24", is_2lep_trig_matched(sl, "HLT_e7_lhmedium_mu24", m_el0, m_mu0, 8, 25));

        ////////////////////////////////////////////////////////////////////////////
        // 2016
        m_triggerPass.emplace("HLT_2e17_lhvloose_nod0", is_2lep_trig_matched(sl, "HLT_2e17_lhvloose_nod0", m_el0, m_el1, 18, 18));
        m_triggerPass.emplace("HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1", is_2lep_trig_matched(sl, "HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1", m_el0, m_mu0, 27, 9));

        ////////////////////////////////////////////////////////////////////////////
        // 2016-2018

        m_triggerPass.emplace("HLT_e26_lhtight_nod0_ivarloose", is_1lep_trig_matched(sl, "HLT_e26_lhtight_nod0_ivarloose", m_triggerLeptons, 27));
        m_triggerPass.emplace("HLT_e60_lhmedium_nod0", is_1lep_trig_matched(sl, "HLT_e60_lhmedium_nod0", m_triggerLeptons, 61));
        m_triggerPass.emplace("HLT_e140_lhloose_nod0", is_1lep_trig_matched(sl, "HLT_e140_lhloose_nod0", m_triggerLeptons, 141));

        m_triggerPass.emplace("HLT_mu26_ivarmedium", is_1lep_trig_matched(sl, "HLT_mu26_ivarmedium", m_triggerLeptons, 27));
        m_triggerPass.emplace("HLT_mu50", is_1lep_trig_matched(sl, "HLT_mu50", m_triggerLeptons, 51));

        m_triggerPass.emplace("HLT_2mu14", is_2lep_trig_matched(sl, "HLT_2mu14", m_mu0, m_mu1, 15, 15));
        m_triggerPass.emplace("HLT_mu22_mu8noL1", is_2lep_trig_matched(sl, "HLT_mu22_mu8noL1", m_mu0, m_mu1, 23, 9));

        m_triggerPass.emplace("HLT_e17_lhloose_nod0_mu14", is_2lep_trig_matched(sl, "HLT_e17_lhloose_nod0_mu14", m_el0, m_mu0, 18, 15));
        m_triggerPass.emplace("HLT_e7_lhmedium_nod0_mu24", is_2lep_trig_matched(sl, "HLT_e7_lhmedium_nod0_mu24", m_el0, m_mu0, 8, 25));

        ////////////////////////////////////////////////////////////////////////////
        // 2017-2018
        m_triggerPass.emplace("HLT_2e24_lhvloose_nod0", is_2lep_trig_matched(sl, "HLT_2e24_lhvloose_nod0", m_el0, m_el1, 25, 25));

        m_triggerPass.emplace("HLT_e26_lhmedium_nod0_mu8noL1", is_2lep_trig_matched(sl, "HLT_e26_lhmedium_nod0_mu8noL1", m_el0, m_mu0, 27, 9));

        ////////////////////////////////////////////////////////////////////////////
        // 2018
        // L1_2EM15VHI was accidentally prescaled in periods B5-B8 of 2017
        // (runs 326834-328393) with an effective reduction of 0.6 fb-1
        m_triggerPass.emplace("HLT_2e17_lhvloose_nod0_L12EM15VHI", is_2lep_trig_matched(sl, "HLT_2e17_lhvloose_nod0_L12EM15VHI", m_el0, m_el1, 18, 18));

        ////////////////////////////////////////////////////////////////////////////
        // Combined triggers
        int year = sl->nt->evt()->treatAsYear;

        bool passSingleLepTrig = false;
        if (year == 2015) {
            passSingleLepTrig |= m_triggerPass.at("HLT_e24_lhmedium_L1EM20VH")
                              || m_triggerPass.at("HLT_e60_lhmedium")
                              || m_triggerPass.at("HLT_e120_lhloose")
                              || m_triggerPass.at("HLT_mu20_iloose_L1MU15")
                              || m_triggerPass.at("HLT_mu40");
        } else if (year == 2016 || year == 2017 || year == 2018) {
            passSingleLepTrig |= m_triggerPass.at("HLT_e26_lhtight_nod0_ivarloose")
                              || m_triggerPass.at("HLT_e60_lhmedium_nod0")
                              || m_triggerPass.at("HLT_e140_lhloose_nod0")
                              || m_triggerPass.at("HLT_mu26_ivarmedium")
                              || m_triggerPass.at("HLT_mu50");
        }
        m_triggerPass.emplace("singleLepTrigs", passSingleLepTrig);

        bool passDilepTrig = false;
        if (year == 2015) {
            passDilepTrig |= m_triggerPass.at("HLT_2e12_lhloose_L12EM10VH")
                          || m_triggerPass.at("HLT_2mu10")
                          || m_triggerPass.at("HLT_mu18_mu8noL1")
                          || m_triggerPass.at("HLT_e17_lhloose_mu14")
                          || m_triggerPass.at("HLT_e7_lhmedium_mu24");
        } else if (year == 2016) {
            passDilepTrig |= m_triggerPass.at("HLT_2e17_lhvloose_nod0")
                          || m_triggerPass.at("HLT_mu22_mu8noL1")
                          || m_triggerPass.at("HLT_2mu14")
                          || m_triggerPass.at("HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1")
                          || m_triggerPass.at("HLT_e17_lhloose_nod0_mu14")
                          || m_triggerPass.at("HLT_e7_lhmedium_nod0_mu24");
        } else if (year == 2017) {
            passDilepTrig |= m_triggerPass.at("HLT_2e24_lhvloose_nod0")
                          || m_triggerPass.at("HLT_mu22_mu8noL1")
                          || m_triggerPass.at("HLT_2mu14")
                          || m_triggerPass.at("HLT_e26_lhmedium_nod0_mu8noL1")
                          || m_triggerPass.at("HLT_e17_lhloose_nod0_mu14")
                          || m_triggerPass.at("HLT_e7_lhmedium_nod0_mu24");
        } else if (year == 2018) {
            passDilepTrig |= m_triggerPass.at("HLT_2e24_lhvloose_nod0")
                          || m_triggerPass.at("HLT_2e17_lhvloose_nod0_L12EM15VHI")
                          || m_triggerPass.at("HLT_mu22_mu8noL1")
                          || m_triggerPass.at("HLT_2mu14")
                          || m_triggerPass.at("HLT_e17_lhloose_nod0_mu14")
                          || m_triggerPass.at("HLT_e26_lhmedium_nod0_mu8noL1")
                          || m_triggerPass.at("HLT_e7_lhmedium_nod0_mu24");
        }
        m_triggerPass.emplace("dilepTrigs", passDilepTrig);

        bool passTrig = passSingleLepTrig || passDilepTrig;
        m_triggerPass.emplace("lepTrigs", passTrig);


        ////////////////////////////////////////////////////////////////////////
        return true; // All events pass this cut
    };
}
void add_cleaning_cuts(Superflow* sf) {
    *sf << CutName("Pass GRL") << [](Superlink* sl) -> bool {
        return (sl->tools->passGRL(m_cutflags));
    };
    *sf << CutName("Error flags") << [](Superlink* sl) -> bool {
        return (sl->tools->passLarErr(m_cutflags)
                && sl->tools->passTileErr(m_cutflags)
                && sl->tools->passSCTErr(m_cutflags)
                && sl->tools->passTTC(m_cutflags));
    };
    *sf << CutName("pass Good Vertex") << [](Superlink * sl) -> bool {
        return (sl->tools->passGoodVtx(m_cutflags));
    };
    *sf << CutName("pass bad muon veto") << [](Superlink* sl) -> bool {
        return (sl->tools->passBadMuon(sl->preMuons));
    };
    *sf << CutName("pass cosmic muon veto") << [](Superlink* sl) -> bool {
        return (sl->tools->passCosmicMuon(sl->baseMuons));
    };
    *sf << CutName("pass jet cleaning") << [](Superlink* sl) -> bool {
        return (sl->tools->passJetCleaning(sl->baseJets));
    };
}
void add_analysis_cuts(Superflow* sf) {
    *sf << CutName("exactly two base leptons") << [](Superlink* sl) -> bool {
        return sl->baseLeptons->size() == 2;
    };

    *sf << CutName("m_ll > 20 GeV") << [](Superlink* sl) -> bool {
        return (*sl->baseLeptons->at(0) + *sl->baseLeptons->at(1)).M() > 20.0;
    };

    *sf << CutName("tau veto") << [](Superlink* sl) -> bool {
        return sl->taus->size() == 0;
    };

    *sf << CutName("exactly two signal leptons") << [](Superlink* sl) -> bool {
        return (sl->leptons->size() == 2);
    };

    *sf << CutName("opposite sign") << [](Superlink* /*sl*/) -> bool {
        return (m_lept1->q * m_lept2->q < 0);
    };
}
void add_event_variables(Superflow* sf) {
    *sf << NewVar("event weight (multi period)"); {
        *sf << HFTname("eventweight_multi");
        *sf << [](Superlink* sl, var_double*) -> double {
            return sl->weights->susynt_multi
                 * sl->weights->lepSf
                 * sl->weights->jvtSf
                 * sl->weights->btagSf
                 * sl->weights->trigSf
                 * sl->nt->evt()->wPileup // multi-period pileup weight
                 ;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("event weight (single period)"); {
        *sf << HFTname("eventweight_single");
        *sf << [](Superlink* sl, var_double*) -> double {
            return sl->weights->susynt
                 * sl->weights->lepSf
                 * sl->weights->jvtSf
                 * sl->weights->btagSf
                 * sl->weights->trigSf
                 * (sl->nt->evt()->wPileup / sl->nt->evt()->wPileup_period)
                 ;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("Period weight"); {
        *sf << HFTname("period_weight");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->nt->evt()->wPileup_period;};
        *sf << SaveVar();
    }

    *sf << NewVar("Monte-Carlo generator event weight"); {
        *sf << HFTname("w");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->nt->evt()->w;};
        *sf << SaveVar();
    }

    // Scale Factors
    *sf << NewVar("Pile-up weight (Multi-period)"); {
        *sf << HFTname("pupw_multi");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->nt->evt()->wPileup;};
        *sf << SaveVar();
    }

    *sf << NewVar("Pile-up weight (Single-period)"); {
        *sf << HFTname("pupw_single");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->nt->evt()->wPileup / sl->nt->evt()->wPileup_period;};
        *sf << SaveVar();
    }

    *sf << NewVar("Lepton scale factor"); {
        *sf << HFTname("lepSf");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->weights->lepSf;};
        *sf << SaveVar();
    }

    *sf << NewVar("Trigger scale factor"); {
        *sf << HFTname("trigSf");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->weights->trigSf;};
        *sf << SaveVar();
    }

    *sf << NewVar("B-tag scale factor"); {
        *sf << HFTname("btagSf");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->weights->btagSf;};
        *sf << SaveVar();
    }

    *sf << NewVar("JVT scale factor"); {
        *sf << HFTname("jvtSf");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->weights->jvtSf;};
        *sf << SaveVar();
    }

    // Event identifiers
    *sf << NewVar("Event run number"); {
        *sf << HFTname("runNumber");
        *sf << [](Superlink* sl, var_int*) -> int { return sl->nt->evt()->run; };
        *sf << SaveVar();
    }

    *sf << NewVar("Event number"); {
        *sf << HFTname("eventNumber");
        *sf << [](Superlink* sl, var_int*) -> int { return sl->nt->evt()->eventNumber; };
        *sf << SaveVar();
    }

    *sf << NewVar("lumi block"); {
        *sf << HFTname("lb");
        *sf << [](Superlink* sl, var_int*) -> int { return sl->nt->evt()->lb; };
        *sf << SaveVar();
    }

    *sf << NewVar("is Monte Carlo"); {
        *sf << HFTname("isMC");
        *sf << [](Superlink* sl, var_bool*) -> bool { return sl->isMC; };
        *sf << SaveVar();
    }

    *sf << NewVar("mcChannel (dsid)"); {
        *sf << HFTname("dsid");
        *sf << [](Superlink* sl, var_double*) -> double {return sl->isMC ? sl->nt->evt()->mcChannel : 0.0;};
        *sf << SaveVar();
    }

    *sf << NewVar("treatAsYear"); {
        *sf << HFTname("treatAsYear");
        *sf << [](Superlink* sl, var_int*) -> int { return sl->nt->evt()->treatAsYear; };
        *sf << SaveVar();
    }

    // Event properties
    *sf << NewVar("Average Mu"); {
        *sf << HFTname("avgMu");
        *sf << [](Superlink* sl, var_double*) -> double { return sl->nt->evt()->avgMu; };
        *sf << SaveVar();
    }

    *sf << NewVar("Average Mu (Data DF)"); {
        *sf << HFTname("avgMuDataSF");
        *sf << [](Superlink* sl, var_double*) -> double { return sl->nt->evt()->avgMuDataSF; };
        *sf << SaveVar();
    }

    *sf << NewVar("Actual Mu"); {
        *sf << HFTname("actualMu");
        *sf << [](Superlink* sl, var_double*) -> double { return sl->nt->evt()->actualMu; };
        *sf << SaveVar();
    }

    *sf << NewVar("Actual Mu (Data SF)"); {
        *sf << HFTname("actualMuDataSF");
        *sf << [](Superlink* sl, var_double*) -> double { return sl->nt->evt()->actualMuDataSF; };
        *sf << SaveVar();
    }

    *sf << NewVar("Number of vertices"); {
        *sf << HFTname("nVtx");
        *sf << [](Superlink* sl, var_double*) -> double { return sl->nt->evt()->nVtx; };
        *sf << SaveVar();
    }

    *sf << NewVar("Number of tracks associated with primary vertex"); {
        *sf << HFTname("nTracksAtPV");
        *sf << [](Superlink* sl, var_double*) -> double { return sl->nt->evt()->nTracksAtPV; };
        *sf << SaveVar();
    }
}
void add_trigger_variables(Superflow* sf) {
    ////////////////////////////////////////////////////////////////////////////
    // Trigger Variables
    // ADD_*_TRIGGER_VAR preprocessor defined

    ////////////////////////////////////////////////////////////////////////////
    // 2015
    ADD_LEP_TRIGGER_VAR(HLT_e24_lhmedium_L1EM20VH)
    ADD_LEP_TRIGGER_VAR(HLT_e60_lhmedium)
    ADD_LEP_TRIGGER_VAR(HLT_e120_lhloose)

    ADD_LEP_TRIGGER_VAR(HLT_2e12_lhloose_L12EM10VH)

    ADD_LEP_TRIGGER_VAR(HLT_mu20_iloose_L1MU15)
    ADD_LEP_TRIGGER_VAR(HLT_mu40)

    ADD_LEP_TRIGGER_VAR(HLT_2mu10)
    ADD_LEP_TRIGGER_VAR(HLT_mu18_mu8noL1)

    ADD_LEP_TRIGGER_VAR(HLT_e17_lhloose_mu14)
    ADD_LEP_TRIGGER_VAR(HLT_e7_lhmedium_mu24)

    ////////////////////////////////////////////////////////////////////////////
    // 2016
    ADD_LEP_TRIGGER_VAR(HLT_2e17_lhvloose_nod0)
    ADD_LEP_TRIGGER_VAR(HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1)

    ////////////////////////////////////////////////////////////////////////////
    // 2016-2018

    ADD_LEP_TRIGGER_VAR(HLT_e26_lhtight_nod0_ivarloose)
    ADD_LEP_TRIGGER_VAR(HLT_e60_lhmedium_nod0)
    ADD_LEP_TRIGGER_VAR(HLT_e140_lhloose_nod0)

    ADD_LEP_TRIGGER_VAR(HLT_mu26_ivarmedium)
    ADD_LEP_TRIGGER_VAR(HLT_mu50)

    ADD_LEP_TRIGGER_VAR(HLT_2mu14)
    ADD_LEP_TRIGGER_VAR(HLT_mu22_mu8noL1)

    ADD_LEP_TRIGGER_VAR(HLT_e17_lhloose_nod0_mu14)
    ADD_LEP_TRIGGER_VAR(HLT_e7_lhmedium_nod0_mu24)

    ////////////////////////////////////////////////////////////////////////////
    // 2017-2018
    ADD_LEP_TRIGGER_VAR(HLT_2e24_lhvloose_nod0)

    ADD_LEP_TRIGGER_VAR(HLT_e26_lhmedium_nod0_mu8noL1)

    ////////////////////////////////////////////////////////////////////////////
    // 2018
    // L1_2EM15VHI was accidentally prescaled in periods B5-B8 of 2017
    // (runs 326834-328393) with an effective reduction of 0.6 fb-1
    ADD_LEP_TRIGGER_VAR(HLT_2e17_lhvloose_nod0_L12EM15VHI)

    ////////////////////////////////////////////////////////////////////////////
    // Combined trigger

    *sf << NewVar("Pass single lepton triggers"); {
        *sf << HFTname("passSingleLepTrigs");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool {
            return m_triggerPass.at("singleLepTrigs");
        };
        *sf << SaveVar();
    }
    *sf << NewVar("Pass dilepton triggers"); {
        *sf << HFTname("passDilepTrigs");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool {
            return m_triggerPass.at("dilepTrigs");
        };
        *sf << SaveVar();
    }
    *sf << NewVar("Pass single or dilepton triggers"); {
        *sf << HFTname("passLepTrigs");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool {
            return m_triggerPass.at("lepTrigs");
        };
        *sf << SaveVar();
    }

}

void add_lepton_variables(Superflow* sf) {
    *sf << NewVar("lepton-1 Pt"); {
        *sf << HFTname("lept1Pt");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept1->Pt(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-1 Eta"); {
        *sf << HFTname("lept1Eta");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept1->Eta(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-1 Phi"); {
        *sf << HFTname("lept1Phi");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept1->Phi(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-1 Energy"); {
        *sf << HFTname("lept1E");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept1->E(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-1 charge"); {
        *sf << HFTname("lept1q");
        *sf << [](Superlink* /*sl*/, var_int*) -> int { return m_lept1->q; };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-1 flavor"); { // 0=el, 1=mu, see HistFitterTree.h
        *sf << HFTname("lept1Flav");
        *sf << [](Superlink* /*sl*/, var_int*) -> int { return m_lept1->isEle() ? 0 : 1; };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-2 Pt"); {
        *sf << HFTname("lept2Pt");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept2->Pt(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-2 Eta"); {
        *sf << HFTname("lept2Eta");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept2->Eta(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-2 Phi"); {
        *sf << HFTname("lept2Phi");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept2->Phi(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-2 Energy"); {
        *sf << HFTname("lept2E");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_lept2->E(); };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-2 charge"); {
        *sf << HFTname("lept2q");
        *sf << [](Superlink* /*sl*/, var_int*) -> int { return m_lept2->q; };
        *sf << SaveVar();
    }

    *sf << NewVar("lepton-2 flavor"); {
        *sf << HFTname("lept2Flav");
        *sf << [](Superlink* /*sl*/, var_int*) -> int { return m_lept2->isEle() ? 0 : 1; };
        *sf << SaveVar();
    }
}
void add_jet_variables(Superflow* sf) {
    *sf << NewVar("number of light jets"); {
        *sf << HFTname("nLightJets");
        *sf << [](Superlink* /*sl*/, var_int*) -> int {return m_light_jets.size(); };
        *sf << SaveVar();
    }

    *sf << NewVar("number of b-tagged jets"); {
        *sf << HFTname("nBJets");
        *sf << [](Superlink* sl, var_int*) -> int { return sl->tools->numberOfBJets(*sl->jets); };
        *sf << SaveVar();
    }

    *sf << NewVar("number of forward jets"); {
        *sf << HFTname("nForwardJets");
        *sf << [](Superlink* sl, var_int*) -> int { return sl->tools->numberOfFJets(*sl->jets); };
        *sf << SaveVar();
    }

    *sf << NewVar("jet-1 Pt"); {
        *sf << HFTname("jet1Pt");
        *sf << [](Superlink* sl, var_float*) -> double {
            return sl->jets->size() >= 1 ? sl->jets->at(0)->Pt() : -DBL_MAX;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("jet-1 Eta"); {
        *sf << HFTname("jet1Eta");
        *sf << [](Superlink* sl, var_float*) -> double {
            return sl->jets->size() >= 1 ? sl->jets->at(0)->Eta() : -DBL_MAX;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("jet-1 Phi"); {
        *sf << HFTname("jet1Phi");
        *sf << [](Superlink* sl, var_float*) -> double {
            return sl->jets->size() >= 1 ? sl->jets->at(0)->Eta() : -DBL_MAX;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("jet-2 Pt"); {
        *sf << HFTname("jet2Pt");
        *sf << [](Superlink* sl, var_float*) -> double {
            return sl->jets->size() >= 2 ? sl->jets->at(1)->Pt() : -DBL_MAX;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("jet-2 Eta"); {
        *sf << HFTname("jet2Eta");
        *sf << [](Superlink* sl, var_float*) -> double {
            return sl->jets->size() >= 2 ? sl->jets->at(1)->Eta() : -DBL_MAX;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("jet-2 Phi"); {
        *sf << HFTname("jet2Phi");
        *sf << [](Superlink* sl, var_float*) -> double {
            return sl->jets->size() >= 2 ? sl->jets->at(1)->Phi() : -DBL_MAX;
        };
        *sf << SaveVar();
    }
}
void add_met_variables(Superflow* sf) {
    *sf << NewVar("transverse missing energy (Et)"); {
        *sf << HFTname("met");
        *sf << [](Superlink* sl, var_float*) -> double { return sl->met->Et; };
        *sf << SaveVar();
    }

    *sf << NewVar("transverse missing energy (Phi)"); {
        *sf << HFTname("metPhi");
        *sf << [](Superlink* sl, var_float*) -> double { return sl->met->phi; };
        *sf << SaveVar();
    }

    *sf << NewVar("Etmiss Rel"); {
        *sf << HFTname("metrel");
        *sf << [](Superlink* sl, var_float*) -> double { return kin::getMetRel(sl->met, *sl->leptons, *sl->jets); };
        *sf << SaveVar();
    }

    *sf << NewVar("delta Phi of leading lepton and met"); {
        *sf << HFTname("deltaPhi_met_l1");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return abs(m_lept1->DeltaPhi(m_MET)); };
        *sf << SaveVar();
    }

    *sf << NewVar("delta Phi of subleading lepton and met"); {
        *sf << HFTname("deltaPhi_met_l2");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return abs(m_lept2->DeltaPhi(m_MET)); };
        *sf << SaveVar();
    }
}
void add_dilepton_variables(Superflow* sf) {
    *sf << NewVar("is e + e"); {
        *sf << HFTname("isElEl");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool { return m_lept1->isEle() && m_lept2->isEle(); };
        *sf << SaveVar();
    }

    *sf << NewVar("is e + mu"); {
        *sf << HFTname("isElMu");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool { return m_lept1->isEle() ^ m_lept2->isEle(); };
        *sf << SaveVar();
    }

    *sf << NewVar("is mu + mu"); {
        *sf << HFTname("isMuMu");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool { return m_lept1->isMu() && m_lept2->isMu(); };
        *sf << SaveVar();
    }

    *sf << NewVar("is opposite-sign"); {
        *sf << HFTname("isOS");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool { return m_lept1->q * m_lept2->q < 0; };
        *sf << SaveVar();
    }

    *sf << NewVar("is Mu (lead) + E (sub)"); {
        *sf << HFTname("isME");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool { return m_lept1->isMu() && m_lept2->isEle(); };
        *sf << SaveVar();
    }

    *sf << NewVar("is E (lead) + Mu (sub)"); {
        *sf << HFTname("isEM");
        *sf << [](Superlink* /*sl*/, var_bool*) -> bool { return m_lept1->isEle() && m_lept2->isMu(); };
        *sf << SaveVar();
    }

    *sf << NewVar("mass of di-lepton system"); {
        *sf << HFTname("mll");
        *sf << [](Superlink* /*sl*/, var_float*) -> double {return m_dileptonP4.M();};
        *sf << SaveVar();
    }

    *sf << NewVar("Pt of di-lepton system"); {
        *sf << HFTname("pTll");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return m_dileptonP4.Pt(); };
        *sf << SaveVar();
    }

    *sf << NewVar("delta Eta of di-lepton system"); {
        *sf << HFTname("deta_ll");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return abs(m_lept1->Eta() - m_lept2->Eta()); };
        *sf << SaveVar();
    }

    *sf << NewVar("delta Phi of di-lepton system"); {
        *sf << HFTname("dphi_ll");
        *sf << [](Superlink* /*sl*/, var_float*) -> double { return abs(m_lept1->DeltaPhi(*m_lept2)); };
        *sf << SaveVar();
    }
}
void add_super_razor_variables(Superflow* sf) {
    *sf << NewVar("stransverse mass"); {
        *sf << HFTname("MT2");
        *sf << [](Superlink* sl, var_float*) -> double {
            double mt2_ = kin::getMT2(*sl->leptons, *sl->met);
            return mt2_;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("Super-Razor Var: shatr"); {
        *sf << HFTname("shatr");
        *sf << [](Superlink* sl, var_float*) -> double {
            double   mDeltaR              = 0.0;
            double   SHATR                = 0.0;
            double   cosThetaRp1          = 0.0;
            double   dphi_LL_vBETA_T      = 0.0;
            double   dphi_L1_L2           = 0.0;
            double   gamma_R              = 0.0;
            double   dphi_vBETA_R_vBETA_T = 0.0;
            TVector3 vBETA_z;
            TVector3 pT_CM;
            TVector3 vBETA_T_CMtoR;
            TVector3 vBETA_R;
            kin::superRazor(*sl->leptons, sl->met, vBETA_z, pT_CM,
                                    vBETA_T_CMtoR, vBETA_R, SHATR, dphi_LL_vBETA_T,
                                    dphi_L1_L2, gamma_R, dphi_vBETA_R_vBETA_T,
                                    mDeltaR, cosThetaRp1);
            return SHATR;
        };
        *sf << SaveVar();
    }

    *sf << NewVar("Super-Razor Var: mDeltaR"); {
        *sf << HFTname("mDeltaR");
        *sf << [](Superlink* sl, var_double*) -> double {
            double   mDeltaR              = 0.0;
            double   SHATR                = 0.0;
            double   cosThetaRp1          = 0.0;
            double   dphi_LL_vBETA_T      = 0.0;
            double   dphi_L1_L2           = 0.0;
            double   gamma_R              = 0.0;
            double   dphi_vBETA_R_vBETA_T = 0.0;
            TVector3 vBETA_z;
            TVector3 pT_CM;
            TVector3 vBETA_T_CMtoR;
            TVector3 vBETA_R;
            kin::superRazor(*sl->leptons, sl->met, vBETA_z, pT_CM,
                                    vBETA_T_CMtoR, vBETA_R, SHATR, dphi_LL_vBETA_T,
                                    dphi_L1_L2, gamma_R, dphi_vBETA_R_vBETA_T,
                                    mDeltaR, cosThetaRp1);
            return mDeltaR;
        };
        *sf << SaveVar();
    }
}
void add_miscellaneous_variables(Superflow* sf) {
    *sf << NewVar("Ht (m_Eff: lep + met + jet)"); {
        *sf << HFTname("ht");
        *sf << [](Superlink* sl, var_float*) -> double {
            double ht = 0.0;

            ht += m_lept1->Pt() + m_lept2->Pt();
            ht += sl->met->Et;
            for (int i = 0; i < (int)sl->jets->size(); i++) {
                if (sl->jets->at(i)->Pt() > 20.0) {
                    ht += sl->jets->at(i)->Pt();
                }
            }
            return ht;
        };
        *sf << SaveVar();
    }
}
void add_weight_systematics(Superflow* sf) {
    *sf << NewSystematic("shift in electron ID efficiency"); {
        *sf << WeightSystematic(SupersysWeight::EL_EFF_ID_TOTAL_Uncorr_UP, SupersysWeight::EL_EFF_ID_TOTAL_Uncorr_DN);
        *sf << TreeName("EL_EFF_ID");
        *sf << SaveSystematic();
    }
    *sf << NewSystematic("shift in electron ISO efficiency"); {
        *sf << WeightSystematic(SupersysWeight::EL_EFF_Iso_TOTAL_Uncorr_UP, SupersysWeight::EL_EFF_Iso_TOTAL_Uncorr_DN);
        *sf << TreeName("EL_EFF_Iso");
        *sf << SaveSystematic();
    }
    *sf << NewSystematic("shift in electron RECO efficiency"); {
        *sf << WeightSystematic(SupersysWeight::EL_EFF_Reco_TOTAL_Uncorr_UP, SupersysWeight::EL_EFF_Reco_TOTAL_Uncorr_DN);
        *sf << TreeName("EL_EFF_Reco");
        *sf << SaveSystematic();
    }
}
void add_shape_systematics(Superflow* sf) {
    *sf << NewSystematic("shift in e-gamma resolution (UP)"); {
        *sf << EventSystematic(NtSys::EG_RESOLUTION_ALL_UP);
        *sf << TreeName("EG_RESOLUTION_ALL_UP");
        *sf << SaveSystematic();
    }
    *sf << NewSystematic("shift in e-gamma resolution (DOWN)"); {
        *sf << EventSystematic(NtSys::EG_RESOLUTION_ALL_DN);
        *sf << TreeName("EG_RESOLUTION_ALL_DN");
        *sf << SaveSystematic();
    }
    *sf << NewSystematic("shift in e-gamma scale (UP)"); {
        *sf << EventSystematic(NtSys::EG_SCALE_ALL_UP);
        *sf << TreeName("EG_SCALE_ALL_UP");
        *sf << SaveSystematic();
    }
}


bool is_1lep_trig_matched(Superlink* sl, string trig_name, LeptonVector leptons, float pt_min) {
    for (Susy::Lepton* lep : leptons) {
        if(!lep) continue;
        if (lep->Pt() < pt_min) continue;
        bool trig_fired = sl->tools->triggerTool().passTrigger(sl->nt->evt()->trigBits, trig_name);
        if (!trig_fired) continue;
        bool trig_matched = sl->tools->triggerTool().lepton_trigger_match(lep, trig_name);
        if (trig_matched) return true;
    }
    return false;
}

bool is_2lep_trig_matched(Superlink* sl, string trig_name, Susy::Lepton* lep1, Susy::Lepton* lep2, float pt_min1, float pt_min2) {
    if(!lep1 || ! lep2) return false;
    if (lep1->Pt() < pt_min1 || lep2->Pt() < pt_min2) return false;
    bool trig_fired = sl->tools->triggerTool().passTrigger(sl->nt->evt()->trigBits, trig_name);
    if (!trig_fired) return false;
    bool trig_matched = sl->tools->triggerTool().dilepton_trigger_match(sl->nt->evt(), lep1, lep2, trig_name);
    return trig_matched;
}



